#ifndef CONTACTLISTENER_HPP
#define CONTACTLISTENER_HPP

#include <Box2D/Box2D.h>
#include "types.hpp"
#include "utils.hpp"

class ContactListener : public b2ContactListener
{
	public:
		void BeginContact(b2Contact* contact);		
		void EndContact(b2Contact* contact);
		void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
		void PostSolve(b2Contact* contact, const b2ContactImpulse* impulse);
};

#endif // CONTACTLISTENER_HPP

