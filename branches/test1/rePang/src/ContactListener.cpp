#include "ContactListener.hpp"

void ContactListener::BeginContact(b2Contact* contact)
{
    B2_NOT_USED(contact);
}

void ContactListener::EndContact(b2Contact* contact)
{
    B2_NOT_USED(contact);
}

void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
    B2_NOT_USED(oldManifold);
	if (contact->GetManifold()->pointCount == 0) return;
    CollisionedObjects.push_back(std::make_pair(contact->GetFixtureA(), contact->GetFixtureB()));
}

void ContactListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{
    B2_NOT_USED(contact);
    B2_NOT_USED(impulse);
}

/*
void ContactListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	const b2Manifold* manifold = contact->GetManifold();
	if (manifold->pointCount == 0) return;
    
	
     b2PointState state1[b2_maxManifoldPoints], state2[b2_maxManifoldPoints];
     b2GetPointStates(state1, state2, oldManifold, manifold);
     b2WorldManifold worldManifold;
     contact->GetWorldManifold(&worldManifold);
     for (int32 i = 0; i < manifold->pointCount; ++i)
     {
     b2Vec2 point = worldManifold.points[i];
     }
     if (checkTypeCollision(contact->GetFixtureA(), contact->GetFixtureB()) == BALL_vs_GROND)
     {
     
     b2Body* ground = getBodyGrond(contact->GetFixtureA(), contact->GetFixtureB());
     b2Body* ball = getBodyBall(contact->GetFixtureA(), contact->GetFixtureB());
     
     setBallImpulse(lastHit, ground, ball);
     lastHit = static_cast<usrdata_t*>(ground->GetUserData())->type;
     }     
}
*/
